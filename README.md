# FRET Software


##Matlab App Designer Project:

Programm for FRET measurements. Used in combination with frequency generators, a iC-HG30 250 MHz Laser Switch + Thorlabs LEDs, PMTs and a Zurich Instruments MFLI 500 kHz.

The software controlls the ic-HG30 and acquires the measurement data from the MFLI 500.
